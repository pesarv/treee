const lightTheme = {
  "--app-bg": "linear-gradient(#fff 12rem, #d8e8ff)",
  "--header-btn-bg": "white",
  "--header-btn-rgb": "45, 80, 125",
  "--header-btn-shadow-rgb": "45, 80, 125",
  "--tree-bg": "white",
  "--contrast": "black",
  "--theme-bg-2": "#fbfdff",
  "--item-bg": "#eff6ff",
  "--item-accent": "#c1c1c1",
  "--font-c": "#484848",
  "--tree-shadow": `0 0 1px rgba(45, 80, 125, 0.05), 0 0 2px rgba(45, 80, 125, 0.05),
  0 0 4px rgba(45, 80, 125, 0.05), 0 0 8px rgba(45, 80, 125, 0.05),
  0 0 16px rgba(45, 80, 125, 0.05), 0 0 32px rgba(45, 80, 125, 0.05),
  0 0 64px rgba(45, 80, 125, 0.05)`,
  "--item-shadow-rgb": "200, 200, 200",
};

const darkTheme = {
  "--app-bg": "linear-gradient(#3a3a48 12rem, #141414)",
  "--header-btn-bg": "#cbcde0",
  "--header-btn-rgb": "73, 73, 89",
  "--header-btn-shadow-rgb": "5, 5, 10",
  "--tree-bg": "#484858",
  "--contrast": "white",
  "--theme-bg-2": "#fbfdff",
  "--item-bg": "#3e3e4c",
  "--item-accent": "#b2b6d2",
  "--font-c": "#eff6ff",
  "--tree-shadow": `0 0 1px rgba(15, 15, 30, 0.2), 0 0 2px rgba(15, 15, 30, 0.2), 0 0 4px rgba(15, 15, 30, 0.2), 0 0 8px rgba(15, 15, 30, 0.2), 0 0 16px rgba(15, 15, 30, 0.2), 0 0 32px rgba(15, 15, 30, 0.2), 0 0 64px rgba(15, 15, 30, 0.2)`,
  "--item-shadow-rgb": "5, 5, 10",
};

enum Theme {
  DARK,
  LIGHT,
}

export { lightTheme, darkTheme, Theme };
