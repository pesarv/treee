import "./App.scss";
import { Tree } from "./components/Tree/Tree";
import { FontAwesomeIcon as FaIcon } from "@fortawesome/react-fontawesome";
import { faMoon, faSun } from "@fortawesome/free-solid-svg-icons";
import { lightTheme, darkTheme, Theme } from "./theme";
import { useState } from "react";
import classnames from "classnames";

function App() {
  const [appTheme, setAppTheme] = useState<Theme>(Theme.LIGHT);

  const changeTheme = () => {
    const nextTheme = appTheme === Theme.LIGHT ? darkTheme : lightTheme;

    for (const customProp in nextTheme) {
      document.documentElement.style.setProperty(
        customProp,
        nextTheme[customProp]
      );
    }

    setAppTheme((prevTheme) =>
      prevTheme === Theme.LIGHT ? Theme.DARK : Theme.LIGHT
    );
  };

  return (
    <div className="app">
      <div className="app__header">
        <h1>Tree</h1>
        <button className="theme-btn" onClick={changeTheme}>
          <FaIcon
            icon={faSun}
            className={classnames("theme-icon", {
              "theme-icon--visible": appTheme === Theme.DARK,
            })}
          ></FaIcon>
          <FaIcon
            icon={faMoon}
            className={classnames("theme-icon", {
              "theme-icon--visible": appTheme === Theme.LIGHT,
            })}
          ></FaIcon>
        </button>
      </div>
      <Tree></Tree>
    </div>
  );
}

export default App;
