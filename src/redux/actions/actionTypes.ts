export interface Action {
  type: Actions;
  payload?: any;
}

export enum Actions {
  ADD_FILE = "ADD_FILE",
  ADD_CATALOG = "ADD_CATALOG",
  DELETE_FILE = "DELETE_FILE",
  DELETE_CATALOG = "DELETE_CATALOG",
  MOVE_ITEM = "MOVE_ITEM",
  RENAME_ITEM = "RENAME_ITEM",
  SET_DRAG_ITEM = "SET_DRAG_ITEM",
  SET_DRAG_TARGET = "SET_DRAG_TARGET",
  SET_RENAME = "SET_RENAME",
}