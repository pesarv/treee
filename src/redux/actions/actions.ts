import { TreeItem } from "../../Types/TreeItem";
import { Actions } from "./actionTypes";

const addFile = (id: string, parentId: string) => {
  return {
    type: Actions.ADD_FILE,
    payload: {
      id,
      parentId,
    },
  };
};

const addCatalog = (id: string, parentId: string) => {
  return {
    type: Actions.ADD_CATALOG,
    payload: {
      id,
      parentId,
    },
  };
};

const deleteFile = (id: string) => {
  return {
    type: Actions.DELETE_FILE,
    payload: {
      id,
    },
  };
};

const deleteCatalog = (id: string) => {
  return {
    type: Actions.DELETE_CATALOG,
    payload: {
      id,
    },
  };
};

const moveItem = (id: string, parentId: string, itemType: TreeItem) => {
  return {
    type: Actions.MOVE_ITEM,
    payload: {
      id,
      parentId,
      itemType,
    },
  };
};

const renameItem = (id: string, name: string) => {
  return {
    type: Actions.RENAME_ITEM,
    payload: {
      id,
      name,
    },
  };
};

const setDragItem = (id: string) => {
  return {
    type: Actions.SET_DRAG_ITEM,
    payload: {
      id,
    },
  };
};

const setDragTarget = (id: string) => {
  return {
    type: Actions.SET_DRAG_TARGET,
    payload: {
      id,
    },
  };
};


const setRename = (id: string) => {
  return {
    type: Actions.SET_RENAME,
    payload: {
      id,
    },
  };
};

export {
  addFile,
  addCatalog,
  deleteFile,
  deleteCatalog,
  moveItem,
  renameItem,
  setDragItem,
  setDragTarget,
  setRename,
};
