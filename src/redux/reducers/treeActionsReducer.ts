import { treeActions } from "../../components/Tree/TreeMock";
import { Action, Actions } from "../actions/actionTypes";
import { createReducer } from "@reduxjs/toolkit";

const initialTreeActionsState = treeActions;

export const treeActionsReducer = createReducer(initialTreeActionsState, {
  [Actions.SET_RENAME]: (state, action: Action) => {
    setRename(state, action.payload);
  },
  [Actions.SET_DRAG_ITEM]: (state, action: Action) => {
    setDragItem(state, action.payload);
  },
  [Actions.SET_DRAG_TARGET]: (state, action: Action) => {
    setDragTarget(state, action.payload);
  },
});

const setRename = (treeActions, payload) => {
  const { id } = payload;
  treeActions.editingItemId = id;
};

const setDragItem = (treeActions, payload) => {
  const { id } = payload;
  treeActions.dragItemId = id;
};

const setDragTarget = (treeActions, payload) => {
  const { id } = payload;
  treeActions.dragTargetId = id;
};