import { treeMock } from "../../components/Tree/TreeMock";
import { Catalog, TreeItem } from "../../Types/TreeItem";
import { Action, Actions } from "../actions/actionTypes";
import { createReducer } from "@reduxjs/toolkit";
import { getCatalogById, getItemById, getParentCatalogByChildId } from "./helpers";

const initialTreeState = treeMock;

export const treeReducer = createReducer(initialTreeState, {
  [Actions.ADD_CATALOG]: (state, action: Action) => {
    addSubCatalog(state, action.payload);
  },
  [Actions.ADD_FILE]: (state, action: Action) => {
    addFile(state, action.payload);
  },
  [Actions.DELETE_CATALOG]: (state, action: Action) => {
    deleteCatalog(state, action.payload);
  },
  [Actions.DELETE_FILE]: (state, action: Action) => {
    deleteFile(state, action.payload);
  },
  [Actions.MOVE_ITEM]: (state, action: Action) => {
    moveItem(state, action.payload);
  },
  [Actions.RENAME_ITEM]: (state, action: Action) => {
    renameItem(state, action.payload);
  },
});

const addFile = (tree: Catalog, payload) => {
  const { id, parentId } = payload;
  const parent = getCatalogById(tree, parentId);
  if (!parent) {
    return;
  }
  parent.files = [...parent.files, { id, name: 'New file' }];
};

const addSubCatalog = (tree: Catalog, payload) => {
  const { id, parentId } = payload;
  const parent = getCatalogById(tree, parentId);
  if (!parent) {
    return;
  }
  parent.subCatalogs = [
    ...parent.subCatalogs,
    { id, name: 'New catalog', files: [], subCatalogs: [] },
  ];
};

const deleteCatalog = (tree: Catalog, payload) => {
  const { id } = payload;
  const parent = getParentCatalogByChildId(tree, id);
  if(!parent) {
    return;
  }
  parent.subCatalogs = parent.subCatalogs.filter((subCatalog) => {
    return subCatalog.id !== id;
  })
}

const deleteFile = (tree: Catalog, payload) => {
  const { id } = payload;
  const parent = getParentCatalogByChildId(tree, id);
  if(!parent) {
    return;
  }
  parent.files = parent.files.filter((file) => {
    return file.id !== id;
  })
}

const moveItem = (tree: Catalog, payload) => {
  const { id, parentId, itemType } = payload;
  const item = getItemById(tree, id);
  const parent = getCatalogById(tree, parentId);
  if (itemType === TreeItem.CATALOG) {
    deleteCatalog(tree, { id })
    parent.subCatalogs = [...parent.subCatalogs, item as Catalog]
  }
  if (itemType === TreeItem.FILE) {
    deleteFile(tree, { id })
    parent.files = [...parent.files, item]
  }
  
}

const renameItem = (tree: Catalog, payload) => {
  const { id, name } = payload;
  const item = getItemById(tree, id);
  if (item) {
    item.name = name;
  }  
} 
