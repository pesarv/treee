import { Catalog, GenericItem } from "../../Types/TreeItem";

export const getCatalogById = (catalog: Catalog, id: string): Catalog => {
  let catalogMatch: Catalog = null;
  if (catalog.id === id) {
    catalogMatch = catalog;
  } else {
    for (const subCatalog of catalog.subCatalogs) {
      catalogMatch = getCatalogById(subCatalog, id);
      if (catalogMatch) {
        break;
      }
    }
  }
  return catalogMatch;
};

export const getItemById = (catalog: Catalog, id: string): GenericItem => {
  let match: GenericItem = null;
  if (catalog.id === id) {
    return catalog;
  }
  const children: GenericItem[] = [...catalog.files, ...catalog.subCatalogs];  
  match = children.find(child => child.id === id);

  if (!match) {
    for (const subCatalog of catalog.subCatalogs) {
      match = getItemById(subCatalog, id);
      if (match) {
        break;
      }
    }
  }
  return match;
};

export const getParentCatalogByChildId = (
  catalog: Catalog,
  childId: string
): Catalog => {
  if (catalog.id === childId) {
    return null;
  }
  let catalogMatch: Catalog = null;
  const children = [...catalog.files, ...catalog.subCatalogs];
  if (children.some((child) => child.id === childId)) {
    catalogMatch = catalog;
  } else {
    for (const subCatalog of catalog.subCatalogs) {
      catalogMatch = getParentCatalogByChildId(subCatalog, childId);
      if (catalogMatch) {
        break;
      }
    }
  }
  return catalogMatch;
};