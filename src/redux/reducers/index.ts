import { combineReducers } from "redux";
import { treeActionsReducer } from "./treeActionsReducer";
import { treeReducer } from './treeReducer'

export const rootReducer = combineReducers({ tree: treeReducer, treeActions: treeActionsReducer });
export type RootState = ReturnType<typeof rootReducer>