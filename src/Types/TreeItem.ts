export interface Catalog extends GenericItem {
  files: File[];
  subCatalogs: Catalog[];
};

export interface File extends GenericItem {};

export interface GenericItem {
  id: string;
  name: string;
}

export enum TreeItem {
  CATALOG = "CATALOG",
  FILE = "FILE",  
}