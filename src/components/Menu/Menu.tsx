import React, { useEffect, useCallback } from "react";
import "./menu.scss";

export interface ICoordinates {
  Y: number;
  X: number;
}

interface IMenu {
  open?: boolean;
  coordinates?: ICoordinates;
  onClose: () => {}
}

export const Menu: React.FC<IMenu> = ({ open, coordinates, onClose, children }) => {
  const { Y, X } = coordinates ?? { Y:0, X:0 };

  const onClick = useCallback(() => {
    window.removeEventListener('click', onClick);
    window.removeEventListener('contextmenu', onClick);
    onClose();
  }, [onClose])

  useEffect(() => {
    if (open) {
      window.addEventListener('click', onClick);
      window.addEventListener('contextmenu', onClick)
    }
  }, [open, onClick]) 

  return (
    <>
      {open && (
        <div className="menu" style={{ top: Y, left: X }}>
          <ul className="menu__list">{children}</ul>
        </div>
      )}
    </>
  );
};
