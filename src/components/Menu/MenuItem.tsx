import React from 'react'

interface IMenuItem {
  onClick?: () => void;
}

export const MenuItem: React.FC<IMenuItem> = ({onClick, children}) => {
  return (
    <li onClick={onClick} className="menu__item">
      {children}
    </li>
  )
}
