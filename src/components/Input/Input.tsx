import React, { useState } from "react";
import "./input.scss";
import classNames from "classnames";

export const Input = ({ label = null, onChange, onBlur = null, value }) => {
  const [hasFocus, setHasFocus] = useState(false);

  const handleKeyPress = (e) => {
    const { key } = e;
    if (key === "Enter") {
      e.target.blur();
    }
  } 

  return (
    <div className={classNames('tree-input', {'tree-input--active': hasFocus || !!value})} >
      {label && <label className="tree-input__label">{label}</label>}
      <input
        autoFocus
        value={value}
        className="tree-input__input"
        onChange={(e) => {
          onChange(e);
        }}
        onFocus={() => {
          setHasFocus(true);
        }}
        onBlur={() => {
          setHasFocus(false);
          onBlur();
        }}
        onKeyPress={handleKeyPress}
      ></input>
    </div>
  );
};