import {
  faFileAlt,
  faFolder,
  faFolderPlus,
  faPen,
  faTrashAlt,
} from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon as FaIcon } from "@fortawesome/react-fontawesome";
import React, { useCallback, useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import {
  addCatalog,
  addFile,
  deleteCatalog,
  deleteFile,
  moveItem,
  renameItem,
  setDragItem,
  setDragTarget,
  setRename,
} from "../../redux/actions/actions";
import { RootState } from "../../redux/reducers";
import { Catalog, File, GenericItem, TreeItem } from "../../Types/TreeItem";
import { Input } from "../Input/Input";
import { ICoordinates, Menu } from "../Menu/Menu";
import { MenuItem } from "../Menu/MenuItem";
import "./tree-item.scss";
import { v4 as uuid } from "uuid";
import classNames from "classnames";

interface ICatalogItem {
  catalog: Catalog;
}

interface IFileItem {
  file: File;
}

interface ITreeItemHead {
  icon: JSX.Element;
  item: GenericItem;
  onContextMenu?: (event) => void;
}

const dragStartHandler = (
  e: React.DragEvent,
  id: string,
  itemType: TreeItem
) => {
  e.stopPropagation();
  e.dataTransfer.setData("dragItemId", JSON.stringify({ id, itemType }));
};

const CatalogItem: React.FC<ICatalogItem> = ({ catalog }) => {
  const [menuCoordinates, setMenuCoordinates] = useState<ICoordinates>();
  const [showMenu, setShowMenu] = useState<boolean>(false);
  const [isMounted, setisMounted] = useState(true);
  const dispatch = useDispatch();
  const treeActions = useSelector((state: RootState) => state.treeActions);

  useEffect(() => {
    return () => {
      setisMounted(false);
    };
  }, []);

  const hasChildren = !(!catalog.files.length && !catalog.subCatalogs.length);

  const onContextMenu = (e: MouseEvent) => {
    e.preventDefault();
    setMenuCoordinates({ X: e.clientX, Y: e.clientY });
    setShowMenu(true);
  };

  const onMenuClose = useCallback(() => {
    if (isMounted) {
      setShowMenu(false);
    }
  }, [isMounted]);

  const onDrop = (e: React.DragEvent, parentId: string) => {
    e.preventDefault();
    e.stopPropagation();
    const { id, itemType } = JSON.parse(e.dataTransfer.getData("dragItemId"));
    dispatch(moveItem(id, parentId, itemType));
    dispatch(setDragTarget(null));
  };

  const onDropCapture = (e: React.DragEvent, id: string) => {
    const { id: dropItemId } = JSON.parse(e.dataTransfer.getData("dragItemId"));
    if (dropItemId === id) {
      e.stopPropagation();
    }
  };

  const onDragOverCapture = (e: React.DragEvent, id: string) => {
    e.preventDefault();
    if (treeActions.dragItemId === e.currentTarget.id) {
      dispatch(setDragTarget(id));
      e.stopPropagation();
    }
    if (id === e.currentTarget.id) {
      dispatch(setDragTarget(id));
    }
  };

  return (
    <div
      id={catalog.id}
      className={classNames("catalog", {
        "catalog--target": treeActions.dragTargetId === catalog.id,
      })}
      draggable
      onDragStart={(e: React.DragEvent) => {
        dispatch(setDragItem(catalog.id));
        dragStartHandler(e, catalog.id, TreeItem.CATALOG);
      }}
      onDragOverCapture={(e) => {
        onDragOverCapture(e, catalog.id);
      }}
      onDrop={(e) => {
        dispatch(setDragItem(null));
        onDrop(e, catalog.id);
      }}
      onDropCapture={(e) => {
        onDropCapture(e, catalog.id);
      }}
      onDragEnd={() => {
        dispatch(setDragItem(null));
        dispatch(setDragTarget(null));
      }}
    >
      <TreeItemHead
        onContextMenu={onContextMenu}
        icon={<FaIcon icon={faFolder}></FaIcon>}
        item={catalog}
      ></TreeItemHead>
      {hasChildren && (
        <div className="catalog__children">
          {catalog.subCatalogs.map((subCatalog) => (
            <CatalogItem key={subCatalog.id} catalog={subCatalog}></CatalogItem>
          ))}
          {catalog.files.map((file) => (
            <FileItem key={file.id} file={file}></FileItem>
          ))}
        </div>
      )}
      <CatalogItemMenu
        catalog={catalog}
        onMenuClose={onMenuClose}
        open={showMenu}
        coordinates={menuCoordinates}
      ></CatalogItemMenu>
    </div>
  );
};

const CatalogItemMenu = ({ catalog, open, coordinates, onMenuClose }) => {
  const dispatch = useDispatch();

  return (
    <Menu open={open} coordinates={coordinates} onClose={onMenuClose}>
      <MenuItem
        onClick={() => {
          const id = uuid();
          dispatch(addCatalog(id, catalog.id));
          dispatch(setRename(id));
        }}
      >
        <FaIcon icon={faFolderPlus}></FaIcon> Add subcatalog
      </MenuItem>
      <MenuItem
        onClick={() => {
          const id = uuid();
          dispatch(addFile(id, catalog.id));
          dispatch(setRename(id));
        }}
      >
        <FaIcon icon={faFileAlt}></FaIcon> Add file
      </MenuItem>
      <MenuItem
        onClick={() => {
          dispatch(deleteCatalog(catalog.id));
        }}
      >
        <FaIcon icon={faTrashAlt}></FaIcon> Delete
      </MenuItem>
      <MenuItem
        onClick={() => {
          dispatch(setRename(catalog.id));
        }}
      >
        <FaIcon icon={faPen}></FaIcon> Rename
      </MenuItem>
    </Menu>
  );
};

const FileItem: React.FC<IFileItem> = ({ file }) => {
  const [menuCoordinates, setMenuCoordinates] = useState<ICoordinates>();
  const [showMenu, setShowMenu] = useState<boolean>(false);
  const [isMounted, setisMounted] = useState(true);
  const dispatch = useDispatch();

  useEffect(() => {
    return () => {
      setisMounted(false);
    };
  }, []);

  const onContextMenu = (e: React.MouseEvent) => {
    e.preventDefault();
    setMenuCoordinates({ X: e.clientX, Y: e.clientY });
    setShowMenu(true);
  };

  const onMenuClose = useCallback(() => {
    if (isMounted) {
      setShowMenu(false);
    }
  }, [isMounted]);

  return (
    <div
      id={file.id}
      className="file"
      draggable
      onDragStart={(e: React.DragEvent) => {
        dispatch(setDragItem(file.id));
        dragStartHandler(e, file.id, TreeItem.FILE);
      }}
      onDragEnd={() => {
        dispatch(setDragItem(null));
        dispatch(setDragTarget(null));
      }}
    >
      <TreeItemHead
        onContextMenu={onContextMenu}
        icon={<FaIcon icon={faFileAlt}></FaIcon>}
        item={file}
      ></TreeItemHead>
      <FileItemMenu
        file={file}
        onMenuClose={onMenuClose}
        open={showMenu}
        coordinates={menuCoordinates}
      ></FileItemMenu>
    </div>
  );
};

const FileItemMenu = ({ file, open, coordinates, onMenuClose }) => {
  const dispatch = useDispatch();

  return (
    <Menu open={open} coordinates={coordinates} onClose={onMenuClose}>
      <MenuItem
        onClick={() => {
          dispatch(deleteFile(file.id));
        }}
      >
        <FaIcon icon={faTrashAlt}></FaIcon> Delete
      </MenuItem>
      <MenuItem
        onClick={() => {
          dispatch(setRename(file.id));
        }}
      >
        <FaIcon icon={faPen}></FaIcon> Rename
      </MenuItem>
    </Menu>
  );
};

const TreeItemHead: React.FC<ITreeItemHead> = ({
  item,
  icon,
  onContextMenu,
}) => {
  const [newLabel, setNewLabel] = useState(item.name);
  const treeActions = useSelector((state: RootState) => state.treeActions);
  const dispatch = useDispatch();

  return (
    <div
      className={classNames("tree-item", {
        "tree-item--target": treeActions.dragTargetId === item.id,
      })}
      onContextMenu={onContextMenu}
    >
      <div className="tree-item__icon">{icon}</div>
      {treeActions.editingItemId === item.id ? (
        <Input
          value={newLabel}
          onChange={(e) => {
            setNewLabel(e.target.value);
          }}
          onBlur={() => {
            dispatch(renameItem(item.id, newLabel));
            dispatch(setRename(null));
          }}
        ></Input>
      ) : (
        <div className="tree-item__label">{item.name}</div>
      )}
    </div>
  );
};

export { CatalogItem, FileItem };
