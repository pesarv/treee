import { Catalog } from "../../Types/TreeItem";

export const treeMock: Catalog = {
  id: "catalog-1",
  name: "Root",
  files: [
    {
      id: "file-1",
      name: "File",
    },
  ],
  subCatalogs: [
    {
      id: "catalog-2",
      name: "Subcatalog",
      files: [],
      subCatalogs: [],
    },
    {
      id: "catalog-3",
      name: "Subcatalog",
      files: [
        {
          id: "file-3",
          name: "Document",
        },
      ],
      subCatalogs: [
        {
          id: "catalog-4",
          name: "Subcatalog",
          files: [],
          subCatalogs: [],
        },
      ],
    },
  ],
};

export const treeActions = {
  editingItemId: null,
  dragItemId: null,
  dragTargetId: null,
}