import React from 'react'
import { useSelector } from 'react-redux'
import { RootState } from '../../redux/reducers'
import { Catalog } from '../../Types/TreeItem'
import { CatalogItem } from '../TreeItem/TreeItem'
import './tree.scss'

const Tree: React.FC = () => {

  const tree: Catalog = useSelector((state: RootState) => state.tree);

  return (
    <div className="tree">
      <CatalogItem catalog={tree}></CatalogItem>
    </div>
  )
}

export { Tree }
